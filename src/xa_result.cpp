#include "defines.h"
#include "xa_result.h"

void* qpidc_xa_result_new(uint16_t status)
{
	return new qpid::framing::XaResult(status);
}

void qpidc_xa_result_delete(void* xa_result)
{
	delete XA_RESULT(xa_result);
}

void qpidc_xa_result_set_status(void* xa_result, uint16_t status)
{
	XA_RESULT(xa_result)->setStatus(status);
}

uint16_t qpidc_xa_result_get_status(void* xa_result)
{
	return XA_RESULT(xa_result)->getStatus();
}

int qpidc_xa_result_has_status(void* xa_result)
{
	return XA_RESULT(xa_result)->hasStatus();
}

void qpidc_xa_result_clear_status_flag(void* xa_result)
{
	XA_RESULT(xa_result)->clearStatusFlag();
}

void qpidc_xa_result_encode(void* xa_result, void* buffer)
{
	XA_RESULT(xa_result)->encode(
		*BUFFER(buffer));
}

void qpidc_xa_result_decode(void* xa_result, void* buffer, uint32_t some_param)
{
	XA_RESULT(xa_result)->decode(
		*BUFFER(buffer),
		some_param);
}

void qpidc_xa_result_encode_struct_body(void* xa_result, void* buffer)
{
	XA_RESULT(xa_result)->encodeStructBody(
		*BUFFER(buffer));
}

void qpidc_xa_result_decode_struct_body(void* xa_result, void* buffer,
					uint32_t some_param)
{
	XA_RESULT(xa_result)->decodeStructBody(
		*BUFFER(buffer),
		some_param);
}

uint32_t qpidc_xa_result_encoded_size(void* xa_result)
{
	return XA_RESULT(xa_result)->encodedSize();
}

uint32_t qpidc_xa_result_body_size(void* xa_result)
{
	return XA_RESULT(xa_result)->bodySize();
}

void qpidc_xa_result_print(void *xa_result)
{
	XA_RESULT(xa_result)->print(std::cout);
}

//
