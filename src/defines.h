#ifndef DEFINES_H
#define DEFINES_H

/*
#include <qpid/client/arg.h>
#include <qpid/client/AsyncSession_0_10.h>
#include <qpid/client/AsyncSession.h>
#include <qpid/client/ClientImportExport.h>
#include <qpid/client/Completion.h>
*/
#include <qpid/client/Connection.h>
/*
#include <qpid/client/ConnectionSettings.h>
#include <qpid/client/FailoverManager.h>
#include <qpid/client/FlowControl.h>
#include <qpid/client/FutureCompletion.h>
#include <qpid/client/Future.h>
#include <qpid/client/FutureResult.h>
#include <qpid/client/Handle.h>
#include <qpid/client/LocalQueue.h>
#include <qpid/client/Message.h>
#include <qpid/client/MessageListener.h>
#include <qpid/client/MessageReplayTracker.h>
#include <qpid/client/QueueOptions.h>
#include <qpid/client/Session_0_10.h>
#include <qpid/client/SessionBase_0_10.h>
#include <qpid/client/Session.h>
#include <qpid/client/Subscription.h>
#include <qpid/client/SubscriptionManager.h>
#include <qpid/client/SubscriptionSettings.h>
#include <qpid/client/TypedResult.h>
*/

#define BUFFER(X) ((qpid::framing::Buffer*)X)
#define CONNECTION(X) ((qpid::client::Connection*)X)
#define DELIVERY_PROPERTIES(X) ((qpid::framing::DeliveryProperties*)X)
#define FIELD_TABLE(X) ((qpid::framing::FieldTable*)X)
#define MESSAGE(X) ((qpid::client::Message*)X)
#define MESSAGE_ACQUIRE_RESULT(X) ((qpid::framing::MessageAcquireResult*)X)
#define SEQUENCE_NUMBER(X) ((qpid::framing::SequenceNumber*)X)
#define SEQUENCE_SET(X) ((qpid::framing::SequenceSet*)X)
#define SESSION(X) ((qpid::client::Session*)X)
#define SESSION_ID(X) ((qpid::SessionId*)X)
#define XA_RESULT(X) ((qpid::framing::XaResult*)X)
#define XID(X) ((qpid::framing::Xid*)X)
/*
#define FILTER(X) ((qpid::messaging::Filter*)X)
#define DURATION(X) ((qpid::sys::Duration*)X)
#define VARIANTMAP(X) ((qpid::messaging::VariantMap*)X)
*/

static inline qpid::framing::SequenceNumber ensure_sequence_number(void *num)
{
	return num ? *SEQUENCE_NUMBER(num) : qpid::framing::SequenceNumber();
}

static inline qpid::framing::SequenceSet ensure_sequence_set(void *set)
{
	return set ? *SEQUENCE_SET(set) : qpid::framing::SequenceSet();
}

static inline qpid::framing::FieldTable ensure_field_table(void* table)
{
	return table ? *FIELD_TABLE(table) : qpid::framing::FieldTable();
}

static inline qpid::framing::Xid ensure_xid(void* xid)
{
	return xid ? *XID(xid) : qpid::framing::Xid();
}

static inline qpid::client::Message ensure_message(void* msg)
{
	return msg ? *MESSAGE(msg) : qpid::client::Message("");
}

static inline std::string ensure_string(char* str, std::string deflt = "")
{
	return std::string(str ? str : deflt);
}

static inline int ensure_int(int val, int deflt)
{
	return val ? val : deflt;
}

#endif	/* DEFINES_H */
