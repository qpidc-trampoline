#include "defines.h"
#include "message_acquire_result.h"
#include <string.h>

void* qpidc_message_acquire_result_new(void* sequence_set)
{
	return new qpid::framing::MessageAcquireResult(
		ensure_sequence_set(sequence_set));
}
//  MessageAcquireResult()  : flags(0) {}

void qpidc_message_acquire_result_delete(void* message_acquire_result)
{
	delete MESSAGE_ACQUIRE_RESULT(message_acquire_result);
}

void qpidc_message_acquire_result_set_transfers(void* message_acquire_result,
						void* transfers)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->setTransfers(
		ensure_sequence_set(transfers));
}

void* qpidc_message_acquire_result_get_transfers(void* message_acquire_result)
{
	return new qpid::framing::SequenceSet(
		MESSAGE_ACQUIRE_RESULT(message_acquire_result)->getTransfers());
}

int qpidc_message_acquire_result_has_transfers(void* message_acquire_result)
{
	return MESSAGE_ACQUIRE_RESULT(message_acquire_result)->hasTransfers();
}

void qpidc_message_acquire_result_clear_transfers_flag(
	void* message_acquire_result)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->clearTransfersFlag();
}

void qpidc_message_acquire_result_encode(void* message_acquire_result,
					 void* buffer)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->encode(
		*BUFFER(buffer));
}

void qpidc_message_acquire_result_decode(void* message_acquire_result,
					 void* buffer, uint32_t some_param)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->decode(
		*BUFFER(buffer),
		some_param);
}

void qpidc_message_acquire_result_encode_struct_body(
	void* message_acquire_result,
	void* buffer)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->encodeStructBody(
		*BUFFER(buffer));
}

void qpidc_message_acquire_result_decode_struct_body(
	void* message_acquire_result,
	void* buffer, uint32_t some_param)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->decodeStructBody(
		*BUFFER(buffer),
		some_param);
}

uint32_t qpidc_message_acquire_result_encoded_size(void* message_acquire_result)
{
	return MESSAGE_ACQUIRE_RESULT(message_acquire_result)->encodedSize();
}

uint32_t qpidc_message_acquire_result_body_size(void* message_acquire_result)
{
	return MESSAGE_ACQUIRE_RESULT(message_acquire_result)->bodySize();
}

void qpidc_message_acquire_result_print(void *message_acquire_result)
{
	MESSAGE_ACQUIRE_RESULT(message_acquire_result)->print(std::cout);
}
//
