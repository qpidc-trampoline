#ifndef SEQUENCE_NUMBER_H
#define SEQUENCE_NUMBER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void* qpidc_sequence_number_new(uint32_t v);
void qpidc_sequence_number_delete(void* sequence_number);
void* qpidc_sequence_number_inc(void* sequence_number);
void* qpidc_sequence_number_dec(void* sequence_number);
int qpidc_sequence_number_equal(void* sequence_number, void* other);
int qpidc_sequence_number_less(void* sequence_number, void* other);
uint32_t qpidc_sequence_number_get_value(void* sequence_number);
void qpidc_sequence_number_encode(void* sequence_number, void* buffer);
void qpidc_sequence_number_decode(void* sequence_number, void* buffer);
uint32_t qpidc_sequence_number_encoded_size(void* sequence_number);
uint32_t qpidc_sequence_number_sub(void* sequence_number, void* s);
void qpidc_sequence_number_print(void* sequence_number);

#ifdef __cplusplus
}
#endif

#endif	/* SEQUENCE_NUMBER_H */
