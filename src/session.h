#ifndef SESSION_H
#define SESSION_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void qpidc_session_delete(void* session);
void* qpidc_session_get_id(void* session);
void qpidc_session_close(void* session);
void qpidc_session_sync(void* session);
uint32_t qpidc_session_timeout(void* session, uint32_t seconds);
void qpidc_session_suspend(void* session);
void qpidc_session_resume(void* session, void* connection);
uint16_t qpidc_session_get_channel(void* session);
void qpidc_session_flush(void* session);
void qpidc_session_mark_completed_sequece_set(void* session, void* ids,
					      int notify_peer);
void qpidc_session_mark_completed_sequece_number(void* session, void* id,
						 int cumulative,
						 int notify_peer);
void qpidc_session_send_completion(void* session);
int qpidc_session_is_valid(void* session);

void qpidc_session_execution_sync(void* session, int sync);
void qpidc_session_execution_result(void* session,
				    void* sequence_number,
				    char* value, int sync);
void qpidc_session_execution_exception(void* session, uint16_t error_code,
				       void* sequence_number,
				       uint8_t class_code,
				       uint8_t command_code,
				       uint8_t field_index,
				       char* description,
				       void* error_info,
				       int sync);
void qpidc_session_message_transfer(void* session, char* destination,
				    uint8_t accept_mode,
				    uint8_t acquire_mode,
				    void* message,
				    int sync);
void qpidc_session_message_accept(void* session, void* transfers, int sync);
void qpidc_session_message_reject(void* session, void* transfers, uint16_t code,
				  char* text, int sync);
void qpidc_session_message_release(void* session, void* transfers,
				   int set_redelivered, int sync);
void* qpidc_session_message_acquire(void* session, void* transfers, int sync);
void* qpidc_session_message_resume(void* session, char* destination,
				   char* resume_id,
				   int sync);
void qpidc_session_message_subscribe(void* session, char* queue,
				     char* destination,
				     uint8_t accept_mode,
				     uint8_t acquire_mode,
				     int exclusive,
				     char* resume_id,
				     uint64_t resume_ttl,
				     void* arguments,
				     int sync);
void qpidc_session_message_cancel(void* session, char* destination, int sync);
void qpidc_session_message_set_flow_mode(void* session, char* destination,
					 uint8_t flow_mode, int sync);
void qpidc_session_message_flow(void* session, char* destination, uint8_t unit,
				uint32_t value, int sync);
void qpidc_session_message_flush(void* session, char* destination, int sync);
void qpidc_session_message_stop(void* session, char* destination, int sync);
void qpidc_session_tx_select(void* session, int sync);
void qpidc_session_tx_commit(void* session, int sync);
void qpidc_session_tx_rollback(void* session, int sync);
void qpidc_session_dtx_select(void* session, int sync);
void* qpidc_session_dtx_start(void* session, void* xid, int join, int resume,
			      int sync);
void* qpidc_session_dtx_end(void* session, void* xid, int fail, int suspend,
			    int sync);
void* qpidc_session_dtx_commit(void* session, void* xid, int one_phase,
			      int sync);
void qpidc_session_dtx_forget(void* session, void* xid, int sync);
void* qpidc_session_dtx_get_timeout(void* session, void* xid, int sync);
void* qpidc_session_dtx_prepare(void* session, void* xid, int sync);
void* qpidc_session_dtx_recover(void* session, int sync);
void* qpidc_session_dtx_rollback(void* session, void* xid, int sync);
void qpidc_session_dtx_set_timeout(void* session, void* xid, uint32_t timeout,
				   int sync);
void qpidc_session_exchange_declare(void* session, char* exchange, char* type,
				    char* alternate_exchange, int passive,
				    int durable, int auto_delete,
				    void* arguments, int sync);
void qpidc_session_exchange_delete(void* session, char* exchange, int if_unused,
				   int sync);
void* qpidc_session_exchange_query(void* session, char* name, int sync);
void qpidc_session_exchange_bind(void* session, char* queue, char* exchange,
				 char* binding_key, void* arguments, int sync);
void qpidc_session_exchange_unbind(void* session, char* queue, char* exchange,
				   char* binding_key, int sync);
void* qpidc_session_exchange_bound(void* session, char* exchange, char* queue,
				   char* binding_key, void* arguments,
				   int sync);
void qpidc_session_queue_declare(void* session, char* queue,
				 char* alternate_exchange, int passive,
				 int durable, int exclusive, int auto_delete,
				 void* arguments, int sync);
void qpidc_session_queue_delete(void* session, char* queue, int if_unused,
				int if_empty, int sync);
void qpidc_session_queue_purge(void* session, char* queue, int sync);
void* qpidc_session_queue_query(void* session, char* queue, int sync);

#ifdef __cplusplus
}
#endif

#endif	/* SESSION_H */
