#ifndef MESSAGE_ACQUIRE_RESULT_H
#define MESSAGE_ACQUIRE_RESULT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int qpidc_message_acquire_result_type = 1028;

void* qpidc_message_acquire_result_new(void* sequence_set);
void qpidc_message_acquire_result_delete(void* message_acquire_result);
void qpidc_message_acquire_result_set_transfers(void* message_acquire_result,
						void* transfers);
void* qpidc_message_acquire_result_get_transfers(void* message_acquire_result);
int qpidc_message_acquire_result_has_transfers(void* message_acquire_result);
void qpidc_message_acquire_result_clear_transfers_flag(
	void* message_acquire_result);
void qpidc_message_acquire_result_encode(void* message_acquire_result,
					 void* buffer);
void qpidc_message_acquire_result_decode(void* message_acquire_result,
					 void* buffer, uint32_t some_param);
void qpidc_message_acquire_result_encode_struct_body(
	void* message_acquire_result,
	void* buffer);
void qpidc_message_acquire_result_decode_struct_body(
	void* message_acquire_result,
	void* buffer, uint32_t some_param);
uint32_t qpidc_message_acquire_result_encoded_size(void* message_acquire_result);
uint32_t qpidc_message_acquire_result_body_size(void* message_acquire_result);
void qpidc_message_acquire_result_print(void *message_acquire_result);

#ifdef __cplusplus
}
#endif

#endif	/* MESSAGE_ACQUIRE_RESULT_H */
