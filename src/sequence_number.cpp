#include "defines.h"
#include "sequence_number.h"

void* qpidc_sequence_number_new(uint32_t v)
{
	return new qpid::framing::SequenceNumber(v);
}

void qpidc_sequence_number_delete(void* sequence_number)
{
	delete SEQUENCE_NUMBER(sequence_number);
}

void* qpidc_sequence_number_inc(void* sequence_number)
{
	(*SEQUENCE_NUMBER(sequence_number))++;
	return sequence_number;
}

void* qpidc_sequence_number_dec(void* sequence_number)
{
	(*SEQUENCE_NUMBER(sequence_number))--;
	return sequence_number;
}

int qpidc_sequence_number_equal(void* sequence_number, void* other)
{
	return *SEQUENCE_NUMBER(sequence_number) ==
		*SEQUENCE_NUMBER(other);
}

int qpidc_sequence_number_less(void* sequence_number, void* other)
{
	return *SEQUENCE_NUMBER(sequence_number) <
		*SEQUENCE_NUMBER(other);
}

uint32_t qpidc_sequence_number_get_value(void* sequence_number)
{
	return SEQUENCE_NUMBER(sequence_number)->getValue();
}

void qpidc_sequence_number_encode(void* sequence_number, void* buffer)
{
	SEQUENCE_NUMBER(sequence_number)->encode(*BUFFER(buffer));
}

void qpidc_sequence_number_decode(void* sequence_number, void* buffer)
{
	SEQUENCE_NUMBER(sequence_number)->decode(*BUFFER(buffer));
}

uint32_t qpidc_sequence_number_encoded_size(void* sequence_number)
{
	return SEQUENCE_NUMBER(sequence_number)->encodedSize();
}

uint32_t qpidc_sequence_number_sub(void* sequence_number, void* s)
{
	return *SEQUENCE_NUMBER(sequence_number) -
		*SEQUENCE_NUMBER(s);
}

void qpidc_sequence_number_print(void* sequence_number)
{
	std::cout << *SEQUENCE_NUMBER(sequence_number);
}
//
