#include "defines.h"
#include "sequence_set.h"

void* qpidc_sequence_set_new()
{
	return new qpid::framing::SequenceSet();
}

void qpidc_sequence_set_delete(void* sequence_set)
{
	delete SEQUENCE_SET(sequence_set);
}

void qpidc_sequence_set_encode(void* sequence_set, void* buffer)
{
	SEQUENCE_SET(sequence_set)->encode(*BUFFER(buffer));
}

void qpidc_sequence_set_decode(void* sequence_set, void* buffer)
{
	SEQUENCE_SET(sequence_set)->decode(*BUFFER(buffer));
}

uint32_t qpidc_sequence_set_encoded_size(void* sequence_set)
{
	return SEQUENCE_SET(sequence_set)->encodedSize();
}

int qpidc_sequence_set_contains(void* sequence_set, void* sequence_number)
{
	return SEQUENCE_SET(sequence_set)->
		contains(*SEQUENCE_NUMBER(sequence_number));
}

void qpidc_sequence_set_add_number(void* sequence_set,
				   void* sequence_number)
{
	SEQUENCE_SET(sequence_set)->add(*SEQUENCE_NUMBER(sequence_number));
}

void qpidc_sequence_set_add_range(void* sequence_set, void* start, void* finish)
{
	SEQUENCE_SET(sequence_set)->add(*SEQUENCE_NUMBER(start),
					*SEQUENCE_NUMBER(finish));
}

void qpidc_sequence_set_add_set(void* sequence_set, void* set)
{
	SEQUENCE_SET(sequence_set)->add(*SEQUENCE_SET(set));
}

void qpidc_sequence_set_remove_number(void* sequence_set,
				      void* sequence_number)
{
	SEQUENCE_SET(sequence_set)->remove(*SEQUENCE_NUMBER(sequence_number));
}

void qpidc_sequence_set_remove_range(void* sequence_set, void* start,
				     void* finish)
{
	SEQUENCE_SET(sequence_set)->remove(*SEQUENCE_NUMBER(start),
					   *SEQUENCE_NUMBER(finish));
}

void qpidc_sequence_set_remove_set(void* sequence_set, void* set)
{
	SEQUENCE_SET(sequence_set)->remove(*SEQUENCE_SET(set));
}

void qpidc_sequence_set_print(void* sequence_set)
{
	std::cout << *SEQUENCE_SET(sequence_set);
}

//
