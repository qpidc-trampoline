#include "defines.h"
#include "connection.h"

void* qpidc_connection_new()
{
	return new qpid::client::Connection();
}

void qpidc_connection_delete(void* connection)
{
	delete CONNECTION(connection);
}

void qpidc_connection_open(void* connection, char* host, int port, char* uid,
			   char* pwd, char* virtualhost,
			   uint16_t max_frame_size)
{
	CONNECTION(connection)->open(host,
				     ensure_int(port, 5672),
				     ensure_string(uid, "guest"),
				     ensure_string(pwd, "guest"),
				     ensure_string(virtualhost, "/"),
				     ensure_int(max_frame_size, 65535));
}

/*
    QPID_CLIENT_EXTERN void open(const Url& url,
    const std::string& uid = "guest",
    const std::string& pwd = "guest",
    const std::string& virtualhost = "/", uint16_t maxFrameSize=65535);

    QPID_CLIENT_EXTERN void open(const Url& url, const ConnectionSettings& settings);

    QPID_CLIENT_EXTERN void open(const ConnectionSettings& settings);
*/

void qpidc_connection_close(void* connection)
{
	CONNECTION(connection)->close();
}

void* qpidc_connection_new_session(void* connection, char* name,
				   uint32_t timeout)
{
	return new qpid::client::Session(
		CONNECTION(connection)->newSession(ensure_string(name),
						   timeout));
}

void qpidc_connection_resume(void* connection, void* session)
{
	CONNECTION(connection)->resume(*SESSION(session));
}

int qpidc_connection_is_open(void* connection)
{
	return CONNECTION(connection)->isOpen();
}

//QPID_CLIENT_EXTERN std::vector<Url> getKnownBrokers();

void qpidc_connection_register_failure_callback(void* connection, void(*cb)())
{
	CONNECTION(connection)->registerFailureCallback(boost::bind(cb));
}
//
