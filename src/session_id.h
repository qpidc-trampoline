#ifndef SESSION_ID_H
#define SESSION_ID_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void* qpidc_session_id_new(char* user_id, char* name);
void qpidc_session_id_delete(void* session_id);
char* qpidc_session_id_get_user_id(void* session_id);
char* qpidc_session_id_get_name(void* session_id);
char* qpidc_session_id_str(void* session_id);

#ifdef __cplusplus
}
#endif

#endif	/* SESSION_ID_H */
