#include "defines.h"
#include "session_id.h"
#include <string.h>

void* qpidc_session_id_new(char* user_id, char* name)
{
	return new qpid::SessionId(
		ensure_string(user_id),
		ensure_string(name));
}

void qpidc_session_id_delete(void* session_id)
{
	delete SESSION_ID(session_id);
}

char* qpidc_session_id_get_user_id(void* session_id)
{
	char* str = strdup(SESSION_ID(session_id)->getUserId().c_str());
	return str;
}

char* qpidc_session_id_get_name(void* session_id)
{
	char* str = strdup(SESSION_ID(session_id)->getName().c_str());
	return str;
}

char* qpidc_session_id_str(void* session_id)
{
	char* str = strdup(SESSION_ID(session_id)->str().c_str());
	return str;
}

//
