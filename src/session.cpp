#include "defines.h"
#include "session.h"

////
// SessionBase
////

void qpidc_session_delete(void* session)
{
	delete SESSION(session);
}

void* qpidc_session_get_id(void* session)
{
	return new qpid::SessionId(SESSION(session)->getId());
}

void qpidc_session_close(void* session)
{
	SESSION(session)->close();
}

void qpidc_session_sync(void* session)
{
	SESSION(session)->sync();
}

uint32_t qpidc_session_timeout(void* session, uint32_t seconds)
{
	return SESSION(session)->timeout(seconds);
}

void qpidc_session_suspend(void* session)
{
	SESSION(session)->suspend();
}

void qpidc_session_resume(void* session, void* connection)
{
	SESSION(session)->resume(*CONNECTION(connection));
}

uint16_t qpidc_session_get_channel(void* session)
{
	return SESSION(session)->getChannel();
}

void qpidc_session_flush(void* session)
{
	SESSION(session)->flush();
}

void qpidc_session_mark_completed_sequece_set(void* session, void* ids,
					      int notify_peer)
{
	SESSION(session)->markCompleted(
		ensure_sequence_set(ids),
		notify_peer);
}

void qpidc_session_mark_completed_sequece_number(void* session, void* id,
						 int cumulative,
						 int notify_peer)
{
	SESSION(session)->markCompleted(
		ensure_sequence_number(id),
		cumulative,
		notify_peer);
}

void qpidc_session_send_completion(void* session)
{
	SESSION(session)->sendCompletion();
}

int qpidc_session_is_valid(void* session)
{
	return SESSION(session)->isValid();
}

////
void qpidc_session_execution_sync(void* session, int sync)
{
	SESSION(session)->executionSync(sync);
}

void qpidc_session_execution_result(void* session, void* sequence_number,
				    char* value, int sync)
{
	SESSION(session)->executionResult(
		ensure_sequence_number(sequence_number),
		ensure_string(value),
		sync);
}

void qpidc_session_execution_exception(void* session, uint16_t error_code,
				       void* sequence_number,
				       uint8_t class_code,
				       uint8_t command_code,
				       uint8_t field_index,
				       char* description,
				       void* error_info,
				       int sync)
{
	SESSION(session)->executionException(
		error_code,
		ensure_sequence_number(sequence_number),
		class_code, command_code, field_index,
		ensure_string(description),
		ensure_field_table(error_info),
		sync);
}

void qpidc_session_message_transfer(void* session, char* destination,
				    uint8_t accept_mode,
				    uint8_t acquire_mode,
				    void* message,
				    int sync)
{
	SESSION(session)->messageTransfer(
		ensure_string(destination),
		accept_mode,
		acquire_mode,
		ensure_message(message),
		sync);
}

void qpidc_session_message_accept(void* session, void* transfers, int sync)
{
	SESSION(session)->messageAccept(
		ensure_sequence_set(transfers),
		sync);
}

void qpidc_session_message_reject(void* session, void* transfers, uint16_t code,
				  char* text, int sync)
{
	SESSION(session)->messageReject(
		ensure_sequence_set(transfers),
		code,
		ensure_string(text),
		sync);
}

void qpidc_session_message_release(void* session, void* transfers,
				   int set_redelivered, int sync)
{
	SESSION(session)->messageRelease(
		ensure_sequence_set(transfers),
		set_redelivered,
		sync);
}

void* qpidc_session_message_acquire(void* session, void* transfers, int sync)
{
	return new qpid::framing::MessageAcquireResult(
		SESSION(session)->messageAcquire(
			ensure_sequence_set(transfers),
			sync));
}

void* qpidc_session_message_resume(void* session, char* destination,
				   char* resume_id,
				   int sync)
{
	return new qpid::framing::MessageResumeResult(
		SESSION(session)->messageResume(
			ensure_string(destination),
			ensure_string(resume_id),
			sync));
}

void qpidc_session_message_subscribe(void* session, char* queue,
				     char* destination,
				     uint8_t accept_mode,
				     uint8_t acquire_mode,
				     int exclusive,
				     char* resume_id,
				     uint64_t resume_ttl,
				     void* arguments,
				     int sync)
{
	SESSION(session)->messageSubscribe(
		ensure_string(queue),
		ensure_string(destination),
		accept_mode,
		acquire_mode,
		exclusive,
		ensure_string(resume_id),
		resume_ttl,
		ensure_field_table(arguments),
		sync);
}

void qpidc_session_message_cancel(void* session, char* destination, int sync)
{
	SESSION(session)->messageCancel(ensure_string(destination), sync);
}

void qpidc_session_message_set_flow_mode(void* session, char* destination,
					 uint8_t flow_mode, int sync)
{
	SESSION(session)->messageSetFlowMode(
		ensure_string(destination),
		flow_mode,
		sync);
}

void qpidc_session_message_flow(void* session, char* destination, uint8_t unit,
				uint32_t value, int sync)
{
	SESSION(session)->messageFlow(
		ensure_string(destination),
		unit,
		value,
		sync);
}

void qpidc_session_message_flush(void* session, char* destination, int sync)
{
	SESSION(session)->messageFlush(ensure_string(destination), sync);
}

void qpidc_session_message_stop(void* session, char* destination, int sync)
{
	SESSION(session)->messageStop(ensure_string(destination), sync);
}

void qpidc_session_tx_select(void* session, int sync)
{
	SESSION(session)->txSelect(sync);
}

void qpidc_session_tx_commit(void* session, int sync)
{
	SESSION(session)->txCommit(sync);
}

void qpidc_session_tx_rollback(void* session, int sync)
{
	SESSION(session)->txRollback(sync);
}

void qpidc_session_dtx_select(void* session, int sync)
{
	SESSION(session)->dtxSelect(sync);
}

void* qpidc_session_dtx_start(void* session, void* xid, int join, int resume,
			      int sync)
{
	return new qpid::framing::XaResult(
		SESSION(session)->dtxStart(
			ensure_xid(xid),
			join,
			resume,
			sync));
}

void* qpidc_session_dtx_end(void* session, void* xid, int fail, int suspend,
			    int sync)
{
	return new qpid::framing::XaResult(
		SESSION(session)->dtxEnd(
			ensure_xid(xid),
			fail,
			suspend,
			sync));
}

void* qpidc_session_dtx_commit(void* session, void* xid, int one_phase,
			       int sync)
{
	return new qpid::framing::XaResult(
		SESSION(session)->dtxEnd(
			ensure_xid(xid),
			one_phase,
			sync));
}

void qpidc_session_dtx_forget(void* session, void* xid, int sync)
{
	SESSION(session)->dtxForget(ensure_xid(xid), sync);
}

void* qpidc_session_dtx_get_timeout(void* session, void* xid, int sync)
{
	return new qpid::framing::DtxGetTimeoutResult(
		SESSION(session)->dtxGetTimeout(
			ensure_xid(xid),
			sync));
}

void* qpidc_session_dtx_prepare(void* session, void* xid, int sync)
{
	return new qpid::framing::XaResult(
		SESSION(session)->dtxPrepare(
			ensure_xid(xid),
			sync));
}

void* qpidc_session_dtx_recover(void* session, int sync)
{
	return new qpid::framing::DtxRecoverResult(
		SESSION(session)->dtxRecover(sync));
}

void* qpidc_session_dtx_rollback(void* session, void* xid, int sync)
{
	return new qpid::framing::XaResult(
		SESSION(session)->dtxRollback(
			ensure_xid(xid),
			sync));
}

void qpidc_session_dtx_set_timeout(void* session, void* xid, uint32_t timeout,
				   int sync)
{
	SESSION(session)->dtxSetTimeout(
		ensure_xid(xid),
		timeout,
		sync);
}

void qpidc_session_exchange_declare(void* session, char* exchange, char* type,
				    char* alternate_exchange, int passive,
				    int durable, int auto_delete,
				    void* arguments, int sync)
{
	SESSION(session)->exchangeDeclare(
		ensure_string(exchange),
		ensure_string(type),
		ensure_string(alternate_exchange),
		passive,
		durable,
		auto_delete,
		ensure_field_table(arguments),
		sync);
}

void qpidc_session_exchange_delete(void* session, char* exchange, int if_unused,
				   int sync)
{
	SESSION(session)->exchangeDelete(
		ensure_string(exchange),
		if_unused,
		sync);
}

void* qpidc_session_exchange_query(void* session, char* name, int sync)
{
	return new qpid::framing::ExchangeQueryResult(
		SESSION(session)->exchangeQuery(
			ensure_string(name), sync));
}

void qpidc_session_exchange_bind(void* session, char* queue, char* exchange,
				 char* binding_key, void* arguments, int sync)
{
	SESSION(session)->exchangeBind(
		ensure_string(queue),
		ensure_string(exchange),
		ensure_string(binding_key),
		ensure_field_table(arguments),
		sync);
}

void qpidc_session_exchange_unbind(void* session, char* queue, char* exchange,
				   char* binding_key, int sync)
{
	SESSION(session)->exchangeUnbind(
		ensure_string(queue),
		ensure_string(exchange),
		ensure_string(binding_key),
		sync);
}

void* qpidc_session_exchange_bound(void* session, char* exchange, char* queue,
				   char* binding_key, void* arguments, int sync)
{
	return new qpid::framing::ExchangeBoundResult(
		SESSION(session)->exchangeBound(
			ensure_string(exchange),
			ensure_string(queue),
			ensure_string(binding_key),
			ensure_field_table(arguments),
			sync));
}

void qpidc_session_queue_declare(void* session, char* queue,
				 char* alternate_exchange, int passive,
				 int durable, int exclusive, int auto_delete,
				 void* arguments, int sync)
{
	SESSION(session)->queueDeclare(
		ensure_string(queue),
		ensure_string(alternate_exchange),
		passive,
		durable,
		exclusive,
		auto_delete,
		ensure_field_table(arguments),
		sync);
}

void qpidc_session_queue_delete(void* session, char* queue, int if_unused,
				int if_empty, int sync)
{
	SESSION(session)->queueDelete(
		ensure_string(queue),
		if_unused,
		if_empty,
		sync);
}

void qpidc_session_queue_purge(void* session, char* queue, int sync)
{
	SESSION(session)->queuePurge(
		ensure_string(queue),
		sync);
}

void* qpidc_session_queue_query(void* session, char* queue, int sync)
{
	return new qpid::framing::QueueQueryResult(
		SESSION(session)->queueQuery(
			ensure_string(queue),
			sync));
}

//
