#ifndef XA_RESULT_H
#define XA_RESULT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int qpidc_xa_result_type = 1537;

void* qpidc_xa_result_new(uint16_t status);
void qpidc_xa_result_delete(void* xa_result);
void qpidc_xa_result_set_status(void* xa_result, uint16_t status);
uint16_t qpidc_xa_result_get_status(void* xa_result);
int qpidc_xa_result_has_status(void* xa_result);
void qpidc_xa_result_clear_status_flag(void* xa_result);
void qpidc_xa_result_encode(void* xa_result, void* buffer);
void qpidc_xa_result_decode(void* xa_result, void* buffer, uint32_t some_param);
void qpidc_xa_result_encode_struct_body(void* xa_result, void* buffer);
void qpidc_xa_result_decode_struct_body(void* xa_result, void* buffer,
					uint32_t some_param);
uint32_t qpidc_xa_result_encoded_size(void* xa_result);
uint32_t qpidc_xa_result_body_size(void* xa_result);
void qpidc_xa_result_print(void *xa_result);

#ifdef __cplusplus
}
#endif

#endif	/* XA_RESULT_H */
