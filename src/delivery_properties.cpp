#include "defines.h"
#include "delivery_properties.h"
#include <string.h>

void* qpidc_delivery_properties_new()
{
	return new qpid::framing::DeliveryProperties();
}

void qpidc_delivery_properties_delete(void* delivery_properties)
{
	delete DELIVERY_PROPERTIES(delivery_properties);
}

void qpidc_delivery_properties_set_discard_unroutable(void* delivery_properties,
						      int discard_unroutable)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		setDiscardUnroutable(discard_unroutable);
}

bool qpidc_delivery_properties_get_discard_unroutable(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getDiscardUnroutable();
}

void qpidc_delivery_properties_set_immediate(void* delivery_properties,
					     int immediate)
{
	DELIVERY_PROPERTIES(delivery_properties)->setImmediate(immediate);
}

bool qpidc_delivery_properties_get_immediate(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getImmediate();
}

void qpidc_delivery_properties_set_redelivered(void* delivery_properties,
					       int redelivered)
{
	DELIVERY_PROPERTIES(delivery_properties)->setRedelivered(redelivered);
}

bool qpidc_delivery_properties_get_redelivered(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getRedelivered();
}

void qpidc_delivery_properties_set_priority(void* delivery_properties,
					    uint8_t priority)
{
	DELIVERY_PROPERTIES(delivery_properties)->setPriority(priority);
}

uint8_t qpidc_delivery_properties_get_priority(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getPriority();
}

bool qpidc_delivery_properties_has_priority(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasPriority();
}

void qpidc_delivery_properties_clear_priority_flags(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearPriorityFlag();
}

void qpidc_delivery_properties_set_delivery_mode(void* delivery_properties,
						 uint8_t delivery_mode)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		setDeliveryMode(delivery_mode);
}

uint8_t qpidc_delivery_properties_get_delivery_mode(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getDeliveryMode();
}

bool qpidc_delivery_properties_has_delivery_mode(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasDeliveryMode();
}

void qpidc_delivery_properties_clear_delivery_mode_flags(
	void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearDeliveryModeFlag();
}

void qpidc_delivery_properties_set_ttl(void* delivery_properties, uint64_t ttl)
{
	DELIVERY_PROPERTIES(delivery_properties)->setTtl(ttl);
}

uint64_t qpidc_delivery_properties_get_ttl(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getTtl();
}

bool qpidc_delivery_properties_has_ttl(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasTtl();
}

void qpidc_delivery_properties_clear_ttl_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearTtlFlag();
}

void qpidc_delivery_properties_set_timestamp(void* delivery_properties,
					     uint64_t timestamp)
{
	DELIVERY_PROPERTIES(delivery_properties)->setTimestamp(timestamp);
}

uint64_t qpidc_delivery_properties_get_timestamp(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getTimestamp();
}

bool qpidc_delivery_properties_has_timestamp(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasTimestamp();
}

void qpidc_delivery_properties_clear_timestamp_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearTimestampFlag();
}

void qpidc_delivery_properties_set_expiration(void* delivery_properties,
					      uint64_t expiration)
{
	DELIVERY_PROPERTIES(delivery_properties)->setExpiration(expiration);
}

uint64_t qpidc_delivery_properties_get_expiration(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getExpiration();
}

bool qpidc_delivery_properties_has_expiration(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasExpiration();
}

void qpidc_delivery_properties_clear_expiration_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearExpirationFlag();
}

void qpidc_delivery_properties_set_exchange(void* delivery_properties,
					    char* exchange)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		setExchange(ensure_string(exchange));
}

char* qpidc_delivery_properties_get_exchange(void* delivery_properties)
{
	std::string s = DELIVERY_PROPERTIES(delivery_properties)->getExchange();
	return strdup(s.c_str());
}

bool qpidc_delivery_properties_has_exchange(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasExchange();
}

void qpidc_delivery_properties_clear_exchange_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearExchangeFlag();
}

void qpidc_delivery_properties_set_routing_key(void* delivery_properties,
					       char* routing_key)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		setRoutingKey(ensure_string(routing_key));
}

char* qpidc_delivery_properties_get_routing_key(void* delivery_properties)
{
	std::string s = DELIVERY_PROPERTIES(delivery_properties)->
		getRoutingKey();
	return strdup(s.c_str());
}

bool qpidc_delivery_properties_has_routing_key(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasRoutingKey();
}

void qpidc_delivery_properties_clear_routing_key_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearRoutingKeyFlag();
}

void qpidc_delivery_properties_set_resume_id(void* delivery_properties,
					     char* resume_id)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		setResumeId(ensure_string(resume_id));
}

char* qpidc_delivery_properties_get_resume_id(void* delivery_properties)
{
	std::string s = DELIVERY_PROPERTIES(delivery_properties)->
		getResumeId();
	return strdup(s.c_str());
}

bool qpidc_delivery_properties_has_resume_id(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasResumeId();
}

void qpidc_delivery_properties_clear_resume_id_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearResumeIdFlag();
}

void qpidc_delivery_properties_set_resume_ttl(void* delivery_properties,
					      uint64_t resume_ttl)
{
	DELIVERY_PROPERTIES(delivery_properties)->setResumeTtl(resume_ttl);
}

uint64_t qpidc_delivery_properties_get_resume_ttl(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->getResumeTtl();
}

bool qpidc_delivery_properties_has_resume_ttl(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->hasResumeTtl();
}

void qpidc_delivery_properties_clear_resume_ttl_flag(void* delivery_properties)
{
	DELIVERY_PROPERTIES(delivery_properties)->clearResumeTtlFlag();
}

void qpidc_delivery_properties_encode(void* delivery_properties, void* buffer)
{
	DELIVERY_PROPERTIES(delivery_properties)->encode(*BUFFER(buffer));
}

void qpidc_delivery_properties_decode(void* delivery_properties, void* buffer,
				      uint32_t t)
{
	DELIVERY_PROPERTIES(delivery_properties)->decode(*BUFFER(buffer), t);
}

void qpidc_delivery_properties_encode_struct_body(void* delivery_properties,
						  void* buffer)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		encodeStructBody(*BUFFER(buffer));
}

void qpidc_delivery_properties_decode_struct_body(void* delivery_properties,
						  void* buffer, uint32_t t)
{
	DELIVERY_PROPERTIES(delivery_properties)->
		decodeStructBody(*BUFFER(buffer), t);
}

uint32_t qpidc_delivery_properties_encoded_size(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->encodedSize();
}

uint32_t qpidc_delivery_properties_body_size(void* delivery_properties)
{
	return DELIVERY_PROPERTIES(delivery_properties)->bodySize();
}

void qpidc_delivery_properties_print(void* delivery_properties)
{
	std::cout << *DELIVERY_PROPERTIES(delivery_properties);
}
//
