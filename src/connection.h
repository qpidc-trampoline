#ifndef CONNECTION_H
#define CONNECTION_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
	void* qpidc_connection_new();
	void qpidc_connection_delete(void* connection);
	void qpidc_connection_open(void* connection, char* host, int port_,
				   char* uid_, char* pwd_, char* virtualhost_,
				   uint16_t max_frame_size_);
	void qpidc_connection_close(void* connection);
	void* qpidc_connection_new_session(void* connection, char* name_,
					   uint32_t timeout_);
	void qpidc_connection_resume(void* connection, void* session);
	int qpidc_connection_is_open(void* connection);
//QPID_CLIENT_EXTERN std::vector<Url> getKnownBrokers();
	void qpidc_connection_register_failure_callback(void* connection,
							void(*cb)());

/*
QPID_CLIENT_EXTERN void parseOptionString(const std::string&, Variant::Map&);
QPID_CLIENT_EXTERN Variant::Map parseOptionString(const std::string&);
*/
#ifdef __cplusplus
}
#endif

#endif	/* CONNECTION_H */
