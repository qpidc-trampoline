#ifndef BUFFER_H
#define BUFFER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void* qpidc_buffer_new(char* data, uint32_t size);
void qpidc_buffer_delete(void* buffer);
void qpidc_buffer_record(void* buffer);
void qpidc_buffer_restore(void* buffer, int re_record);
void qpidc_buffer_reset(void* buffer);
uint32_t qpidc_buffer_available(void* buffer);
uint32_t qpidc_buffer_get_size(void* buffer);
uint32_t qpidc_buffer_get_position(void* buffer);
char* qpidc_buffer_get_pointer(void* buffer);
void qpidc_buffer_put_octet(void* buffer, uint8_t i);
void qpidc_buffer_put_short(void* buffer, uint16_t i);
void qpidc_buffer_put_long(void* buffer, uint32_t i);
void qpidc_buffer_put_long_long(void* buffer, uint64_t i);
void qpidc_buffer_put_int8(void* buffer, int8_t i);
void qpidc_buffer_put_int16(void* buffer, int16_t i);
void qpidc_buffer_put_int32(void* buffer, int32_t i);
void qpidc_buffer_put_int64(void* buffer, int64_t i);
void qpidc_buffer_put_float(void* buffer, float i);
void qpidc_buffer_put_double(void* buffer, double i);
void qpidc_buffer_put_bin_128(void* buffer, uint8_t* b);
uint8_t qpidc_buffer_get_octet(void* buffer);
uint16_t qpidc_buffer_get_short(void* buffer);
uint32_t qpidc_buffer_get_long(void* buffer);
uint64_t qpidc_buffer_get_long_long(void* buffer);
int8_t qpidc_buffer_get_int8(void* buffer);
int16_t qpidc_buffer_get_int16(void* buffer);
int32_t qpidc_buffer_get_int32(void* buffer);
int64_t qpidc_buffer_get_int64(void* buffer);
float qpidc_buffer_get_float(void* buffer);
double qpidc_buffer_get_double(void* buffer);
void qpidc_buffer_put_short_string(void* buffer, char* s);
void qpidc_buffer_put_medium_string(void* buffer, char* s);
void qpidc_buffer_put_long_string(void* buffer, char* s);
char* qpidc_buffer_get_short_string(void* buffer);
char* qpidc_buffer_get_medium_string(void* buffer);
char* qpidc_buffer_get_long_string(void* buffer);
void qpidc_buffer_get_bin_128(void* buffer, uint8_t* b);
void qpidc_buffer_put_raw_data(void* buffer, uint8_t* data, size_t size);
void qpidc_buffer_get_raw_data(void* buffer, uint8_t* data, size_t size);
void qpidc_buffer_dump(void* buffer);
void qpidc_buffer_print(void* buffer);

#ifdef __cplusplus
}
#endif

#endif	/* BUFFER_H */
