#ifndef DELIVERY_PROPERTIES_H
#define DELIVERY_PROPERTIES_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


void* qpidc_delivery_properties_new();
void qpidc_delivery_properties_delete(void* delivery_properties);
void qpidc_delivery_properties_set_discard_unroutable(void* delivery_properties,
						      int discard_unroutable);
bool qpidc_delivery_properties_get_discard_unroutable(void* delivery_properties);
void qpidc_delivery_properties_set_immediate(void* delivery_properties,
					     int immediate);
bool qpidc_delivery_properties_get_immediate(void* delivery_properties);
void qpidc_delivery_properties_set_redelivered(void* delivery_properties,
					       int redelivered);
bool qpidc_delivery_properties_get_redelivered(void* delivery_properties);
void qpidc_delivery_properties_set_priority(void* delivery_properties,
					    uint8_t priority);
uint8_t qpidc_delivery_properties_get_priority(void* delivery_properties);
bool qpidc_delivery_properties_has_priority(void* delivery_properties);
void qpidc_delivery_properties_clear_priority_flags(void* delivery_properties);
void qpidc_delivery_properties_set_delivery_mode(void* delivery_properties,
						 uint8_t delivery_mode);
uint8_t qpidc_delivery_properties_get_delivery_mode(void* delivery_properties);
bool qpidc_delivery_properties_has_delivery_mode(void* delivery_properties);
void qpidc_delivery_properties_clear_delivery_mode_flags(void* delivery_properties);
void qpidc_delivery_properties_set_ttl(void* delivery_properties, uint64_t ttl);
uint64_t qpidc_delivery_properties_get_ttl(void* delivery_properties);
bool qpidc_delivery_properties_has_ttl(void* delivery_properties);
void qpidc_delivery_properties_clear_ttl_flag(void* delivery_properties);
void qpidc_delivery_properties_set_timestamp(void* delivery_properties,
					     uint64_t timestamp);
uint64_t qpidc_delivery_properties_get_timestamp(void* delivery_properties);
bool qpidc_delivery_properties_has_timestamp(void* delivery_properties);
void qpidc_delivery_properties_clear_timestamp_flag(void* delivery_properties);
void qpidc_delivery_properties_set_expiration(void* delivery_properties,
					      uint64_t expiration);
uint64_t qpidc_delivery_properties_get_expiration(void* delivery_properties);
bool qpidc_delivery_properties_has_expiration(void* delivery_properties);
void qpidc_delivery_properties_clear_expiration_flag(void* delivery_properties);
void qpidc_delivery_properties_set_exchange(void* delivery_properties,
					    char* exchange);
char* qpidc_delivery_properties_get_exchange(void* delivery_properties);
bool qpidc_delivery_properties_has_exchange(void* delivery_properties);
void qpidc_delivery_properties_clear_exchange_flag(void* delivery_properties);
void qpidc_delivery_properties_set_routing_key(void* delivery_properties,
					       char* routing_key);
char* qpidc_delivery_properties_get_routing_key(void* delivery_properties);
bool qpidc_delivery_properties_has_routing_key(void* delivery_properties);
void qpidc_delivery_properties_clear_routing_key_flag(void* delivery_properties);
void qpidc_delivery_properties_set_resume_id(void* delivery_properties,
					     char* resume_id);
char* qpidc_delivery_properties_get_resume_id(void* delivery_properties);
bool qpidc_delivery_properties_has_resume_id(void* delivery_properties);
void qpidc_delivery_properties_clear_resume_id_flag(void* delivery_properties);
void qpidc_delivery_properties_set_resume_ttl(void* delivery_properties,
					      uint64_t resume_ttl);
uint64_t qpidc_delivery_properties_get_resume_ttl(void* delivery_properties);
bool qpidc_delivery_properties_has_resume_ttl(void* delivery_properties);
void qpidc_delivery_properties_clear_resume_ttl_flag(void* delivery_properties);
void qpidc_delivery_properties_encode(void* delivery_properties, void* buffer);
void qpidc_delivery_properties_decode(void* delivery_properties, void* buffer,
				      uint32_t t);
void qpidc_delivery_properties_encode_struct_body(void* delivery_properties,
						  void* buffer);
void qpidc_delivery_properties_decode_struct_body(void* delivery_properties,
						  void* buffer, uint32_t t);
uint32_t qpidc_delivery_properties_encoded_size(void* delivery_properties);
uint32_t qpidc_delivery_properties_body_size(void* delivery_properties);
void qpidc_delivery_properties_print(void* delivery_properties);

#ifdef __cplusplus
}
#endif

#endif	/* DELIVERY_PROPERTIES_H */
