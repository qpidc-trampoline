#ifndef SEQUENCE_SET_H
#define SEQUENCE_SET_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void* qpidc_sequence_set_new();
void qpidc_sequence_set_delete(void* sequence_set);
void qpidc_sequence_set_encode(void* sequence_set, void* buffer);
void qpidc_sequence_set_decode(void* sequence_set, void* buffer);
uint32_t qpidc_sequence_set_encoded_size(void* sequence_set);
int qpidc_sequence_set_contains(void* sequence_set, void* sequence_number);
void qpidc_sequence_set_add_number(void* sequence_set,
				   void* sequence_number);
void qpidc_sequence_set_add_range(void* sequence_set, void* start,
				  void* finish);
void qpidc_sequence_set_add_set(void* sequence_set, void* set);
void qpidc_sequence_set_remove_number(void* sequence_set,
				      void* sequence_number);
void qpidc_sequence_set_remove_range(void* sequence_set, void* start,
				     void* finish);
void qpidc_sequence_set_remove_set(void* sequence_set, void* set);

#ifdef __cplusplus
}
#endif

#endif	/* SEQUENCE_SET_H */
