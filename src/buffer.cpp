#include "defines.h"
#include "buffer.h"
#include <string.h>

void* qpidc_buffer_new(char* data, uint32_t size)
{
	return new qpid::framing::Buffer(data, size);
}

void qpidc_buffer_delete(void* buffer)
{
	delete BUFFER(buffer);
}

void qpidc_buffer_record(void* buffer)
{
	BUFFER(buffer)->record();
}

void qpidc_buffer_restore(void* buffer, int re_record)
{
	BUFFER(buffer)->restore(re_record);
}

void qpidc_buffer_reset(void* buffer)
{
	BUFFER(buffer)->reset();
}

uint32_t qpidc_buffer_available(void* buffer)
{
	return BUFFER(buffer)->available();
}

uint32_t qpidc_buffer_get_size(void* buffer)
{
	return BUFFER(buffer)->getSize();
}

uint32_t qpidc_buffer_get_position(void* buffer)
{
	return BUFFER(buffer)->getPosition();
}

char* qpidc_buffer_get_pointer(void* buffer)
{
	return BUFFER(buffer)->getPointer();
}

void qpidc_buffer_put_octet(void* buffer, uint8_t i)
{
	BUFFER(buffer)->putOctet(i);
}

void qpidc_buffer_put_short(void* buffer, uint16_t i)
{
	BUFFER(buffer)->putShort(i);
}

void qpidc_buffer_put_long(void* buffer, uint32_t i)
{
	BUFFER(buffer)->putLong(i);
}

void qpidc_buffer_put_long_long(void* buffer, uint64_t i)
{
	BUFFER(buffer)->putLongLong(i);
}

void qpidc_buffer_put_int8(void* buffer, int8_t i)
{
	BUFFER(buffer)->putInt8(i);
}

void qpidc_buffer_put_int16(void* buffer, int16_t i)
{
	BUFFER(buffer)->putInt16(i);
}

void qpidc_buffer_put_int32(void* buffer, int32_t i)
{
	BUFFER(buffer)->putInt32(i);
}

void qpidc_buffer_put_int64(void* buffer, int64_t i)
{
	BUFFER(buffer)->putInt64(i);
}

void qpidc_buffer_put_float(void* buffer, float i)
{
	BUFFER(buffer)->putFloat(i);
}

void qpidc_buffer_put_double(void* buffer, double i)
{
	BUFFER(buffer)->putDouble(i);
}

void qpidc_buffer_put_bin_128(void* buffer, uint8_t* b)
{
	BUFFER(buffer)->putBin128(b);
}

uint8_t qpidc_buffer_get_octet(void* buffer)
{
	return BUFFER(buffer)->getOctet();
}

uint16_t qpidc_buffer_get_short(void* buffer)
{
	return BUFFER(buffer)->getShort();
}

uint32_t qpidc_buffer_get_long(void* buffer)
{
	return BUFFER(buffer)->getLong();
}

uint64_t qpidc_buffer_get_long_long(void* buffer)
{
	return BUFFER(buffer)->getLongLong();
}

int8_t qpidc_buffer_get_int8(void* buffer)
{
	return BUFFER(buffer)->getInt8();
}

int16_t qpidc_buffer_get_int16(void* buffer)
{
	return BUFFER(buffer)->getInt16();
}

int32_t qpidc_buffer_get_int32(void* buffer)
{
	return BUFFER(buffer)->getInt32();
}

int64_t qpidc_buffer_get_int64(void* buffer)
{
	return BUFFER(buffer)->getInt64();
}

float qpidc_buffer_get_float(void* buffer)
{
	return BUFFER(buffer)->getFloat();
}

double qpidc_buffer_get_double(void* buffer)
{
	return BUFFER(buffer)->getDouble();
}

void qpidc_buffer_put_short_string(void* buffer, char* s)
{
	BUFFER(buffer)->putShortString(ensure_string(s));
}

void qpidc_buffer_put_medium_string(void* buffer, char* s)
{
	BUFFER(buffer)->putMediumString(ensure_string(s));
}

void qpidc_buffer_put_long_string(void* buffer, char* s)
{
	BUFFER(buffer)->putLongString(ensure_string(s));
}

char* qpidc_buffer_get_short_string(void* buffer)
{
	std::string s;
	BUFFER(buffer)->getShortString(s);
	return strdup(s.c_str());
}

char* qpidc_buffer_get_medium_string(void* buffer)
{
	std::string s;
	BUFFER(buffer)->getMediumString(s);
	return strdup(s.c_str());
}

char* qpidc_buffer_get_long_string(void* buffer)
{
	std::string s;
	BUFFER(buffer)->getLongString(s);
	return strdup(s.c_str());
}

void qpidc_buffer_get_bin_128(void* buffer, uint8_t* b)
{
	BUFFER(buffer)->getBin128(b);
}

void qpidc_buffer_put_raw_data(void* buffer, uint8_t* data, size_t size)
{
	BUFFER(buffer)->putRawData(data, size);
}

void qpidc_buffer_get_raw_data(void* buffer, uint8_t* data, size_t size)
{
	BUFFER(buffer)->getRawData(data, size);
}

void qpidc_buffer_dump(void* buffer)
{
	BUFFER(buffer)->dump(std::cout);
}

void qpidc_buffer_print(void* buffer)
{
	std::cout << *BUFFER(buffer);
}

//
