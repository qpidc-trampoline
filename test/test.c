#include <stdio.h>
#include "connection.h"
#include "session.h"
#include "session_id.h"
#include "message_acquire_result.h"

void foo()
{
	printf("connect failed\n");
}

int main()
{
	void* connection = qpidc_connection_new();
	qpidc_connection_register_failure_callback(connection, foo);
	qpidc_connection_open(connection, "127.0.0.1", 0, 0, 0, 0, 0);
	printf("connection = %p\n", connection);
	void* session = qpidc_connection_new_session(connection, 0, 0);

	void* session_id = qpidc_session_get_id(session);
	char* user_id = qpidc_session_id_get_user_id(session_id);
	char* name = qpidc_session_id_get_name(session_id);
	char* str = qpidc_session_id_str(session_id);
	printf("session = %p, user_id = %s, name = %s, str = %s\n", session, user_id, name, str);
	free(user_id);
	free(name);
	free(str);
	qpidc_session_id_delete(session_id);

	void* mar = qpidc_session_message_acquire(session, NULL, 1);
	qpidc_message_acquire_result_print(mar);
	qpidc_message_acquire_result_delete(mar);

	qpidc_session_queue_declare(session, "test_queue", NULL, 0, 0, 0, 1, NULL, 1);
	qpidc_session_exchange_bind(session, "test_queue", "amq.direct", "routing_key", 0, 1);
	qpidc_connection_close(connection);
	qpidc_session_delete(session);
	return 0;
}
